import pickle
import math
"""채널별로 분류된 recipe discription들의 diversity와 평균 accuracy를 구해서 평균낸 값을 채널별로 출력한 것"""
channel_list = [[1, 0, 0.0 , 0.0], None] # [ channel, count, diversity, accuracy ]
def compareList( keyList , objectList ):
	compareCount = 0
	if len(keyList) == len(objectList):
		for i in range(len(keyList)):
			if(keyList[i] == objectList[i]):
				compareCount += 1
				if compareCount == len(keyList):
					return True
			else:
				return False
	else:
		return False
def maxList( countSen ):
	maxList = []
	for i in range(len( countSen )):
		maxList.append( countSen[i][1] )
	return max(maxList)

def compareID( keyList , wholeList , length ): #List[0] = sentence / List[1]= trigger, action / List[2] = aswRate
	diversityList = []
	wholeawsRate = keyList[2]
	count = 0 #문장의 반복 횟수, 문장이 하나 있다면 0, keyList 포함 x
	i = 1
	countSen = [[keyList[1], 1, keyList[2]],None]
	while(i<length - count):	#length = len(wholeList)
		if keyList [2] == -1:
			print("keyList has no accumulate")
			wholeList = wholeList[1:length]
			return wholeList
		if wholeList[i][2] == -1:  # argv 예외처리
			i += 1
			continue
		if compareList( keyList[1], wholeList[i][1]):# list안에 trigger action이 같으면
			count += 1
			if keyList[3] != wholeList[i][3]:
				print ("channel is different!! ", keyList[3],' ',wholeList[i][3])
			for k in range(len(countSen)):
				if countSen[k] == None:
					countSen.insert(k,[wholeList[i][0], 1, wholeList[i][2]]) # countSen[i][0] = trigger, action countSen[i][1] = frequency
					wholeawsRate += wholeList[i][2]
					break
				elif compareList(wholeList[i][0], countSen[k][0]) : #countSen 에서 동일한 문장이 등장할 경우
					countSen[k][1] += 1
					wholeawsRate += wholeList[i][2]
					break
#insert result from comparing each compareID that are same to countID list
				else :
					continue	

			wholeList.append(wholeList.pop(i))
			i -= 1
		i += 1	

	countSen.pop() #remove 'None'
	countLength = len(countSen)
	Sum = 0
	RatioList = []
	"""빈도수별 비중을 구하고 그것의 평균과 표준편차를 구함"""
	"""합계를 구함"""
	try:
		for i in range(countLength):
			Sum += countSen[i][1]
		"""모든 빈도를 비중으로 만들어 준다"""
		for i in range(countLength):
			RatioList.append( countSen[i][1] / float(Sum) )
		"""비중의 표준편차를 계산한다"""
		average = 1.0 / countLength
		dispersion = 0
		for i in range(len(countSen)):
			dispersion += pow((RatioList[i]- average) , 2)
		dispersion = dispersion / countLength
		stanDev = math.sqrt(dispersion)
		diversityList += [countLength - stanDev,keyList[0], wholeawsRate/ Sum, keyList[3]]
	except:
		diversityList += [countLength,keyList[0], wholeawsRate/ Sum, keyList[3]]

#channel이 같으면 diversity와 accuracy를 각각 더해서 평균을 내는 코드
	for k in range(len(channel_list)):
		if channel_list[k] == None:
			channel_list.insert(k, [diversityList[3], 1, diversityList[0], diversityList[2]])
			break
		elif diversityList[3] == channel_list[k][0]:  # diversityList 에서 동일한 채널이 등장할 경우
			channel_list[k][1] += 1
			channel_list[k][2] += diversityList[0] # diversity
			channel_list[k][3] += diversityList[2] # wholeAswrate
			break
		else:
			continue

	wholeList = wholeList[1:length -count]
	return wholeList

def main():
	wholeList = []
	aswRate = [] #정확도를 담는 List
	data = open("testT.txt")
	data1 = open("edit_MA_repet_output.txt") #정확도 결과파일 경로
	for each_line in data1:
		try:
			(channel, accuracy) = each_line.split()
			aswRate.append([eval(accuracy), eval(channel)])
		except:
			aswRate.append([-1, 0])
	j = 0
	for each_line in data:
		sentence = []
		each_line = each_line.strip()
		(strSentence , strTrigger, strAction)= each_line.split(',')
		sentence = strSentence.split() 
		
		elements = []
		elements.append(sentence) 				 # wholeList[0] = recipe description
		elements.append([strTrigger, strAction]) # wholeList[1] = Ifttt app

		if j == len(aswRate): #add aswRate 
			break;
		elements.append(aswRate[j][0]) # wholeList[2] = accuracy
		elements.append(aswRate[j][1]) # wholeList[3] = channel
		j += 1
		wholeList.append(elements)

	while(True):
		wholeList = compareID(wholeList[0], wholeList, len(wholeList))
		print(len(wholeList))
		if len(wholeList) < 1:
			break
	out = open("testT_channel_diversity_accuracy.txt", 'w')
	channel_list.pop()
	channel_list.sort()
	for i in range(len(channel_list)):
		print("{0:>10} {1:>20.4f} {2:>20.4f}".format(channel_list[i][0], (channel_list[i][2]/ channel_list[i][1]), (channel_list[i][3]/ channel_list[i][1])), file= out)
	out.close()

main()





