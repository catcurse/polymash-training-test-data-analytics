import os
import re
import os.path
import math

def compareList( keyList , objectList ):
	compareCount = 0
	if len(keyList) == len(objectList):
		for i in range(len(keyList)):
			if(keyList[i] == objectList[i]):
				compareCount += 1
				if compareCount == len(keyList):
					return True
			else:
				return False
	else:
		return False


def compareID( keyList , wholeList , length ):
	diversityList = []
	count = 0 #문장의 반복 횟수, 문장이 하나 있다면 0, keyList 포함 x
	i = 1 # i = 0 은 keyList
	countSen = [[keyList[0], 1], None]
	while(i<length - count):	#length = len(wholeList)

		if compareList( keyList[1], wholeList[i][1]): #ifttt 앱이 서로 같을때
			count += 1
			for k in range(len(countSen)):
				if countSen[k] == None:
					countSen.insert(k, [wholeList[i][0], 1])  # countSen[i][0] = trigger, action countSen[i][1] = frequency
					break
				elif compareList(wholeList[i][0], countSen[k][0]):  # countSen 에서 동일한 문장이 등장할 경우
					countSen[k][1] += 1
					break
				# insert result from comparing each compareID that are same to countID list
				else:
					continue

			wholeList.append(wholeList.pop(i))
			i -= 1
		i += 1

	countSen.pop()  # remove 'None'
	countLength = len(countSen)
	Sum = 0
	RatioList = []
	"""빈도수별 비중을 구하고 그것의 평균과 표준편차를 구함"""
	"""합계를 구함"""
	try:
		for i in range(countLength):
			Sum += countSen[i][1]
		"""모든 빈도를 비중으로 만들어 준다"""
		for i in range(countLength):
			RatioList.append(countSen[i][1] / float(Sum))
		"""비중의 표준편차를 계산한다"""
		average = 1.0 / countLength
		dispersion = 0
		for i in range(len(countSen)):
			dispersion += pow((RatioList[i] - average), 2)
		dispersion = dispersion / countLength
		stanDev = math.sqrt(dispersion)
		diversityList.append(countLength - stanDev)
		diversityList.append(keyList[1])
		diversityList.append(count+1)
	except:
		diversityList.append(countLength)
		diversityList.append(keyList[1])
		diversityList.append(count+1)

	wholeList = wholeList[1:length -count]
	return wholeList, diversityList , count+1


def main():
	save_path = os.path.join("C:\\",'Users','hawng','Documents','dataAnalysis_edit_MA','edit_MA_testOutput_d')
	prefix = "edit_MA_trainning_diversity"
	directory = os.path.join("C:\\",'Users','hawng','Documents','dataAnalysis_edit_MA','editchannelTrainningSet')
	channel_accuracy = open("edit_MA_repet_channelAccuracy.txt")
	channel_accuracydict = {}
	for elements in channel_accuracy:
		(key, value) = elements.split()
		channel_accuracydict[key] = value
	for root, dirs, files in os.walk(directory):
		for file in files:
			wholeList = []
			printList = []
			base = os.path.basename(file)
			if file.endswith(".txt"):
				data = open(os.path.join(root, file))
				channel = os.path.splitext(base)[0]
				completeName = os.path.join(save_path , prefix + channel + '.txt')
				output = open(completeName, 'w')
				for elements in data:
					try:
						ID, trash= elements.split('\t')
					except:
						continue
					trigger, action = ID.split('_')
					postag = re.findall(r'\/(.+?)\s', elements)
					sentence = re.findall(r'\s(.+?)\/', elements)
					wholeList.append([sentence ,[trigger,action] ,postag])
				sumsum = 0
				try:
					while (True):
						wholeList,vaguenessList, count = compareID(wholeList[0], wholeList, len(wholeList))
						sumsum += count
						printList.append(vaguenessList)
						if len(wholeList) < 1:
							break
				except:
					print("wholeList is empty")
				outAverage = open("edit_MA_diversity_training_repet_channelAccuracy.txt", 'a')
				try:
					Sum = 0
					for i in range(len(printList)):
						Sum += printList[i][0]
					if channel in channel_accuracydict:
						print(channel, '\t', Sum / len(printList), end=' ', file=outAverage)
						print(channel_accuracydict[channel], file=outAverage)
					else:
						print("break!")
					outAverage.close()
				except:
					print("division by zero")

				printList.sort(reverse=True)
				for i in range(len(printList)):
					print("{0:>60} | diversity: {1:4f} 		count: {2:4}".format("_".join(str(x) for x in printList[i][1]),printList[i][0],printList[i][2]), file=output)
				print("complete!")
				output.close()

main()