import os
import re
import os.path
import math

"""training 채널별 vagueness와 각각의 평균을 구하는 프로그램 + postag"""
def compareList( keyList , objectList ):
	compareCount = 0
	if len(keyList) == len(objectList):
		for i in range(len(keyList)):
			if(keyList[i] == objectList[i]):
				compareCount += 1
				if compareCount == len(keyList):
					return True
			else:
				return False
	else:
		return False


def compareID( keyList , wholeList , length ): #List[0] = sentence / List[1]= trigger, action
	vaguenessList = []
	count = 0 #문장의 반복 횟수, 문장이 하나 있다면 0, keyList 포함 x
	i = 1
	countSen = [[keyList[1], 1], None]
	while(i<length - count):	#length = len(wholeList)

		if compareList( keyList[0], wholeList[i][0]): #문장이 서로 같을때
			count += 1
			for k in range(len(countSen)):
				if countSen[k] == None:
					countSen.insert(k, [wholeList[i][1], 1])  # countSen[i][0] = trigger, action countSen[i][1] = frequency
					break
				elif compareList(wholeList[i][1], countSen[k][0]):  # countSen 에서 동일한 트리거 액션 id가 등장할 경우
					countSen[k][1] += 1
					break
				# insert result from comparing each compareID that are same to countID list
				else:
					continue

			wholeList.append(wholeList.pop(i))
			i -= 1
		i += 1

	"""빈도수별 비중을 구하고 그것의 평균과 표준편차를 구함"""
	countSen.pop()  # remove 'None'
	countLength = len(countSen)
	Sum = 0
	RatioList = []
	try:
		"""합계를 구함"""
		for i in range(countLength):
			Sum += countSen[i][1]
		"""모든 빈도를 비중으로 만들어 준다"""
		for i in range(countLength):
			RatioList.append(countSen[i][1] / float(Sum))
		"""비중의 표준편차를 계산한다"""
		average = 1.0 / countLength
		variance = 0
		for i in range(len(countSen)):
			variance += pow((RatioList[i] - average), 2) /countLength
		standard_deviation = math.sqrt(variance)
		vaguenessList.append(count + 1)
		vaguenessList.append(keyList[0])
		vaguenessList.append(countLength - standard_deviation)
	except:
		vaguenessList.append(count + 1)
		vaguenessList.append(keyList[0])
		vaguenessList.append(countLength)

	wholeList = wholeList[1:length -count]
	return wholeList, vaguenessList

def main():
	directory = os.path.join("C:\\",'Users','hawng','Documents','dataAnalysis_edit_MA','editchannelTrainningSet')
	channel_accuracy = open("testT_channel_vagueness_accuracy.txt")
	channel_accuracydict = {}
	for elements in channel_accuracy:
		elements.strip()
		(key, test_vagueness, value) = elements.split()
		channel_accuracydict[key] = [value, test_vagueness]
	for root, dirs, files in os.walk(directory):
		for file in files:
			wholeList = []
			printList = []
			base = os.path.basename(file)
			if file.endswith(".txt"):
				data = open(os.path.join(root, file))
				channel = os.path.splitext(base)[0]
				for elements in data:
					try:
						ID, trash= elements.split('\t')
					except:
						continue
					trigger, action = ID.split('_')
					postag = re.findall(r'\/(.+?)\s', elements)
					sentence = re.findall(r'\s(.+?)\/', elements)
					wholeList.append([sentence ,[trigger,action] ,postag])
				try:
					while (True):
						wholeList,vaguenessList = compareID(wholeList[0], wholeList, len(wholeList))
						printList.append(vaguenessList)
						if len(wholeList) < 1:
							break
				except:
					print("wholeList is empty")

				outAverage = open("edit_MA_training_testT_channel_vagueness_accuracy_amountTraining.txt",'a')
				try:
					Sum = 0
					for i in range(len(printList)):
						Sum += printList[i][0]
					vagueness_sum = 0
					for i in range(len(printList)):
						vagueness_sum += printList[i][2]
					if channel in channel_accuracydict:
						print(channel+'\t'+str(vagueness_sum/ len(printList)), end='', file=outAverage)
						print('\t'+channel_accuracydict[channel][1]+'\t'+channel_accuracydict[channel][0]+'\t'+str(Sum), file = outAverage)
					else:
						print("break!")
					outAverage.close()
				except:
					print("division by zero")

main()